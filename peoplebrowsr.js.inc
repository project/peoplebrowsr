<?php

/**
 * JavaScript callbacks for the peoplebrowsr module.
 *
 * @file: peoplebrowsr.js.inc
 * @author: Elliott Foster 
 * @copyright: NewMBC 2009
 */

/**
 * peoplebrowsr_js_load - load the peoplebrowsr
 *  block and return the result with AHAH.
 */
function peoplebrowsr_js_load($bid) {
  $result = array(
    'status' => FALSE,
    'data' => t('There was a problem loading the peoplebrowsr block. If this continues contact us here: !contact', 
      array('!contact' => l(variable_get('site_mail', 'help@drupal.org'), 'mailto:' . variable_get('site_mail', 'help@drupal.org'))) 
    ),
  );

  $block = db_fetch_object(db_query("SELECT * FROM {peoplebrowsr} WHERE bid=%d", $bid));
  if ($block) {
    $result['status'] = TRUE;
    $result['data'] = theme('peoplebrowsr_block', $block);
  }

  drupal_json($result);
}
