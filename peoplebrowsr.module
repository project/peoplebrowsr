<?php

/**
 * This module provides an interface for creating
 *  PeopleBrowsr blocks and loading the content
 *  on the page with AJAX.  This way the PeopleBrowsr
 *  iframe won't take down the whole site. 
 *
 * @file: peoplebrowsr.module
 * @author: Elliott Foster
 * @copyright: NewMBC 2009
 */

/**
 * Implementation of hook_perm()
 */
function peoplebrowsr_perm() {
  return array(
    'admin peoplebrowsr',
  );
}

/**
 * Implementation of hook_menu()
 */
function peoplebrowsr_menu() {
  $items = array();

  $items['peoplebrowsr/js/load'] = array(
    'page callback' => 'peoplebrowsr_js_load',
    'page arguments' => array(3),
    'access arguments' => array('access content'),
    'type' => MENU_CALLBACK,
    'file' => 'peoplebrowsr.js.inc',
  );

  $items['admin/build/block/peoplebrowsr'] = array(
    'title' => 'PeopleBrowsr Blocks',
    'page callback' => 'peoplebrowsr_admin',
    'access arguments' => array('admin peoplebrowsr'),
    'type' => MENU_LOCAL_TASK,
    'file' => 'peoplebrowsr.admin.inc',
  );
  $items['admin/build/block/peoplebrowsr/add'] = array(
    'title' => 'Add a peoplebrowsr block',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('peoplebrowsr_add'),
    'access arguments' => array('admin peoplebrowsr'),
    'type' => MENU_CALLBACK,
    'file' => 'peoplebrowsr.admin.inc',
  );
  $items['admin/build/block/peoplebrowsr/%/edit'] = array(
    'title' => 'Edit a peoplebrowsr block',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('peoplebrowsr_edit', 4),
    'access arguments' => array('admin peoplebrowsr'),
    'type' => MENU_CALLBACK,
    'file' => 'peoplebrowsr.admin.inc',
  );
  $items['admin/build/block/peoplebrowsr/%/delete'] = array(
    'title' => 'Edit a peoplebrowsr block',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('peoplebrowsr_delete', 4),
    'access arguments' => array('admin peoplebrowsr'),
    'type' => MENU_CALLBACK,
    'file' => 'peoplebrowsr.admin.inc',
  );

  return $items;
}

/**
 * Implementation of hook_theme()
 */
function peoplebrowsr_theme($existing, $type, $theme, $path) {
  return array(
    'peoplebrowsr_block' => array(
      'arguments' => array('block' => NULL),
      'file' => 'peoplebrowsr.theme.inc',
      'template' => 'theme/peoplebrowsr_block',
    ),
  );
}

/**
 * Implementation of hook_block()
 */
function peoplebrowsr_block($op = 'list', $delta = 0, $edit = array()) {
  if ($op == 'list') {
    $blocks = array();

    $res = db_query("SELECT * FROM {peoplebrowsr}");
    while ($block = db_fetch_object($res)) {
      $blocks[$block->bid] = array(
        'info' => t('peoplebrowsr: @url', array('@url' => $block->url))
      );
    }

    return $blocks;
  }
  else if ($op == 'configure') {
    module_load_include('inc', 'peoplebrowsr', 'peoplebrowsr.admin');
    $form = peoplebrowsr_edit(array(), $delta);
    unset($form['submit']);
    unset($form['cancel']);

    return $form;
  }
  else if ($op == 'save') {
    $block = new stdClass();
    $block->bid = $delta;
    $block->url = $edit['url'];
    $block->width = $edit['width'];
    $block->height = $edit['height'];
    drupal_write_record('peoplebrowsr', $block, array('bid'));
  }
  else if ($op == 'view') {
    // the actual block will be loaded with AHAH so it doesn't
    // slow down the loading of the rest of the page.
    drupal_add_js(drupal_get_path('module', 'peoplebrowsr') . '/js/peoplebrowsr.js');

    $block = array(
      'subject' => '',
      'content' => '<div id="peoplebrowsr-block_' . $delta .'" class="peoplebrowsr-block-wrapper"></div>',
    );

    return $block;
  }
}

