<?php

/**
 * Admin callbacks for the peoplebrowsr module
 *
 * @file: peoplebrowsr.admin.inc
 * @author: Elliott Foster
 * @copyright: NewMBC 2009 
 */

/**
 * peoplebrowsr_admin - return the list of
 *  peoplebrowsr blocks with edit and delete
 *  links.
 */
function peoplebrowsr_admin() {
  $out = '';
  $header = array(
    'url' => t('Widget URL'),
    'width' => t('Width'),
    'height' => t('Height'),
    'options' => t('Options'),
  );
  $blocks = array();

  $res = pager_query("SELECT * FROM {peoplebrowsr}", 20);
  while ($block = db_fetch_object($res)) {
    $blocks[$block->bid] = array(
      'url' => $block->url,
      'width' => $block->width,
      'height' => $block->height,
      'options' => l(t('Edit'), 'admin/build/block/peoplebrowsr/' . $block->bid . '/edit') . ' ' .
        l(t('Delete'), 'admin/build/block/peoplebrowsr/' . $block->bid . '/delete'),
    );
  }

  $out = '<div class="peoplebrowsr-blocks">' .
    theme('table', $header, $blocks) .
  '</div>';
  $out .= '<div class="peoplebrowsr-blocks-pager">' . 
    theme('pager', array(), 20) .
  '</div>';
  $out .= '<div class="peoplebrowsr-add-block">' . l(t('Add another peoplebrowsr block'), 'admin/build/block/peoplebrowsr/add') . '</div>';

  return $out;
}

/**
 * peoplebrowsr_add - return the form to add another
 *  peoplebrowsr block.
 */
function peoplebrowsr_add() {
  $form = array();

  $form['url'] = array(
    '#type' => 'textfield',
    '#title' => t('Widget URL'),
    '#description' => t('Enter the URL for the PeopleBrowsr widget'),
    '#maxlength' => 255,
    '#required' => TRUE,
  );
  $form['width'] = array(
    '#type' => 'textfield',
    '#title' => t('Width'),
    '#default_value' => '100%',
    '#description' => t('Enter the HTML compliant width for the widget, i.e. 100px or 100%'),
    '#maxlength' => 255,
    '#required' => TRUE,
  );
  $form['height'] = array(
    '#type' => 'textfield',
    '#title' => t('Height'),
    '#default_value' => '100%',
    '#description' => t('Enter the HTML compliant height for the widget, i.e. 100px or 100%'),
    '#maxlength' => 255,
    '#required' => TRUE,
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Add'),
  );
  $form['cancel'] = array(
    '#type' => 'markup',
    '#value' => l(t('Cancel'), 'admin/build/block/peoplebrowsr'),
  );

  return $form;
}

function peoplebrowsr_add_validate($form, &$form_state) {
  if (!valid_url($form_state['values']['url'], TRUE)) {
    form_set_error('url', t('You must enter a valid, absolute, url'));
  }
}

function peoplebrowsr_add_submit($form, &$form_state) {
  $block = new stdClass();
  $block->url = $form_state['values']['url'];
  $block->width = $form_state['values']['width'];
  $block->height = $form_state['values']['height'];
  drupal_write_record('peoplebrowsr', $block);

  drupal_set_message(t('Your peoplebrowsr block has been added'));
  $form_state['redirect'] = 'admin/build/block/peoplebrowsr';
}

/**
 * peoplebrowsr_edit - return the form to edit
 *  peoplebrowsr blocks.
 */
function peoplebrowsr_edit($form_state, $bid) {
  $block = db_fetch_object(db_query("SELECT * FROM {peoplebrowsr} WHERE bid=%d", $bid));
  $form = array();

  $form['#block'] = $block;

  $form['url'] = array(
    '#type' => 'textfield',
    '#title' => t('Widget URL'),
    '#default_value' => $block->url,
    '#description' => t('Enter the URL for the PeopleBrowsr widget'),
    '#maxlength' => 255,
    '#required' => TRUE,
  );
  $form['width'] = array(
    '#type' => 'textfield',
    '#title' => t('Width'),
    '#default_value' => $block->width,
    '#description' => t('Enter the HTML compliant width for the widget, i.e. 100px or 100%'),
    '#maxlength' => 255,
    '#required' => TRUE,
  );
  $form['height'] = array(
    '#type' => 'textfield',
    '#title' => t('Height'),
    '#default_value' => $block->height,
    '#description' => t('Enter the HTML compliant height for the widget, i.e. 100px or 100%'),
    '#maxlength' => 255,
    '#required' => TRUE,
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
  $form['cancel'] = array(
    '#type' => 'markup',
    '#value' => l(t('Cancel'), 'admin/build/block/peoplebrowsr'),
  );

  return $form;
}

function peoplebrowsr_edit_validate($form, &$form_state) {
  if (!valid_url($form_state['values']['url'], TRUE)) {
    form_set_error('url', t('You must enter a valid, absolute, url'));
  }
}

function peoplebrowsr_edit_submit($form, &$form_state) {
  $block = new stdClass();
  $block->bid = $form['#block']->bid;
  $block->url = $form_state['values']['url'];
  $block->width = $form_state['values']['width'];
  $block->height = $form_state['values']['height'];
  drupal_write_record('peoplebrowsr', $block, array('bid'));

  drupal_set_message(t('Your block has been updated'));
  $form_state['redirect'] = 'admin/build/block/peoplebrowsr';
}

/**
 * peoplebrowsr_delete - return the form to delete
 *  peoplebrowsr blocks.
 */
function peoplebrowsr_delete($form_state, $bid) {
  $block = db_fetch_object(db_query("SELECT * FROM {peoplebrowsr} WHERE bid=%d", $bid));
  $form = array();

  $form['#block'] = $block;
  $form = confirm_form(
    $form,
    t('Are you sure you want to delete this peoplebrowsr block?'),
    'admin/build/block/peoplebrowsr',
    t('This action cannot be undone'),
    t('Delete'),
    t('Cancel')
  );

  return $form;
}

function peoplebrowsr_delete_submit($form, &$form_state) {
  db_query("DELETE FROM {peoplebrowsr} WHERE bid=%d", $form['#block']->bid);
  drupal_set_message(t('The block has been deleted'));
  $form_state['redirect'] = 'admin/build/block/peoplebrowsr';
}

